#! /usr/bin/env bash
SCRIPT_DIR=`dirname "${PWD}/$0"`
(
cd "${SCRIPT_DIR}/.." || exit 1
####################################################################

kate \
	bindings/c/doc/configure.ac \
	doc/configure.ac \
	configure.ac \
	\
	include/xspf/XspfVersion.h \
	ChangeLog \
	&

####################################################################
)
exit 0
